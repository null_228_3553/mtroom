package edu.hust.mtroom.bean;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class MtApply {
    private String resourceId;

    private String userId;

    private String roomId;

    private String applyTitle;

    private String applyExplain;

    @JSONField (format="HH时mm分")
    private Date applyBegintime;

    @JSONField (format="HH时mm分")
    private Date applyEndtime;

    private String applyContact;

    private String applyRemarks;

    private Date createTime;
    //1：已审核， 2：待审 ，3：驳回 ，4：已撤销
    private String checkStatus;

    private String checkId;

    private String conferree;

    private String addMeeting;

    private String otherMeetingroom;

    private String checkUserId;

    private String checkUserName;

    private Date checkTime;

    private String rejectContent;

    private String isCancel;

    private Short viewCount;

    private String isSign;

    private String meetingId;

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getApplyTitle() {
        return applyTitle;
    }

    public void setApplyTitle(String applyTitle) {
        this.applyTitle = applyTitle;
    }

    public String getApplyExplain() {
        return applyExplain;
    }

    public void setApplyExplain(String applyExplain) {
        this.applyExplain = applyExplain;
    }

    public Date getApplyBegintime() {
        return applyBegintime;
    }

    public void setApplyBegintime(Date applyBegintime) {
        this.applyBegintime = applyBegintime;
    }

    public Date getApplyEndtime() {
        return applyEndtime;
    }

    public void setApplyEndtime(Date applyEndtime) {
        this.applyEndtime = applyEndtime;
    }

    public String getApplyContact() {
        return applyContact;
    }

    public void setApplyContact(String applyContact) {
        this.applyContact = applyContact;
    }

    public String getApplyRemarks() {
        return applyRemarks;
    }

    public void setApplyRemarks(String applyRemarks) {
        this.applyRemarks = applyRemarks;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getCheckId() {
        return checkId;
    }

    public void setCheckId(String checkId) {
        this.checkId = checkId;
    }

    public String getConferree() {
        return conferree;
    }

    public void setConferree(String conferree) {
        this.conferree = conferree;
    }

    public String getAddMeeting() {
        return addMeeting;
    }

    public void setAddMeeting(String addMeeting) {
        this.addMeeting = addMeeting;
    }

    public String getOtherMeetingroom() {
        return otherMeetingroom;
    }

    public void setOtherMeetingroom(String otherMeetingroom) {
        this.otherMeetingroom = otherMeetingroom;
    }

    public String getCheckUserId() {
        return checkUserId;
    }

    public void setCheckUserId(String checkUserId) {
        this.checkUserId = checkUserId;
    }

    public String getCheckUserName() {
        return checkUserName;
    }

    public void setCheckUserName(String checkUserName) {
        this.checkUserName = checkUserName;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getRejectContent() {
        return rejectContent;
    }

    public void setRejectContent(String rejectContent) {
        this.rejectContent = rejectContent;
    }

    public String getIsCancel() {
        return isCancel;
    }

    public void setIsCancel(String isCancel) {
        this.isCancel = isCancel;
    }

    public Short getViewCount() {
        return viewCount;
    }

    public void setViewCount(Short viewCount) {
        this.viewCount = viewCount;
    }

    public String getIsSign() {
        return isSign;
    }

    public void setIsSign(String isSign) {
        this.isSign = isSign;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }
}