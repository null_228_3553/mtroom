package edu.hust.mtroom.bean;

import java.util.Date;
import java.util.List;

public class MtRoom {
    private String resourceId;

    private String roomId;

    private String roomName;

    private String roomLocate;

    private String roomAddress;

    private String roomSize;

    private String roomAccomNum;

    private String roomPhotoUri;
    
    private List<String> imageList;

    private String roomState;

    private String roomUnitId;

    private String roomIstopublic;

    private String roomRemarks;

    private String roomSort;

    private String roomOpenTimeStart;

    private String roomOpenTimeEnd;

    private Date createTime;

    private String signMachine;

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomLocate() {
        return roomLocate;
    }

    public void setRoomLocate(String roomLocate) {
        this.roomLocate = roomLocate;
    }

    public String getRoomAddress() {
        return roomAddress;
    }

    public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }

    public String getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(String roomSize) {
        this.roomSize = roomSize;
    }

    public String getRoomAccomNum() {
        return roomAccomNum;
    }

    public void setRoomAccomNum(String roomAccomNum) {
        this.roomAccomNum = roomAccomNum;
    }

    public String getRoomPhotoUri() {
        return roomPhotoUri;
    }

    public void setRoomPhotoUri(String roomPhotoUri) {
        this.roomPhotoUri = roomPhotoUri;
    }

    public String getRoomState() {
        return roomState;
    }

    public void setRoomState(String roomState) {
        this.roomState = roomState;
    }

    public String getRoomUnitId() {
        return roomUnitId;
    }

    public void setRoomUnitId(String roomUnitId) {
        this.roomUnitId = roomUnitId;
    }

    public String getRoomIstopublic() {
        return roomIstopublic;
    }

    public void setRoomIstopublic(String roomIstopublic) {
        this.roomIstopublic = roomIstopublic;
    }

    public String getRoomRemarks() {
        return roomRemarks;
    }

    public void setRoomRemarks(String roomRemarks) {
        this.roomRemarks = roomRemarks;
    }

    public String getRoomSort() {
        return roomSort;
    }

    public void setRoomSort(String roomSort) {
        this.roomSort = roomSort;
    }

    public String getRoomOpenTimeStart() {
        return roomOpenTimeStart;
    }

    public void setRoomOpenTimeStart(String roomOpenTimeStart) {
        this.roomOpenTimeStart = roomOpenTimeStart;
    }

    public String getRoomOpenTimeEnd() {
        return roomOpenTimeEnd;
    }

    public void setRoomOpenTimeEnd(String roomOpenTimeEnd) {
        this.roomOpenTimeEnd = roomOpenTimeEnd;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSignMachine() {
        return signMachine;
    }

    public void setSignMachine(String signMachine) {
        this.signMachine = signMachine;
    }

	public List<String> getImageList() {
		return imageList;
	}

	public void setImageList(List<String> imageList) {
		this.imageList = imageList;
	}

}