package edu.hust.mtroom.bean;

import java.util.Date;

public class ApplyForm {
    private String resourceId;
    
    private String userId;

    private String roomId;

    private String applyTitle;

    private String applyExplain;

    private Date applyBegintime;

    private Date applyEndtime;

    private String applyContact;

    private String applyRemarks;

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getApplyTitle() {
		return applyTitle;
	}

	public void setApplyTitle(String applyTitle) {
		this.applyTitle = applyTitle;
	}

	public String getApplyExplain() {
		return applyExplain;
	}

	public void setApplyExplain(String applyExplain) {
		this.applyExplain = applyExplain;
	}

	public Date getApplyBegintime() {
		return applyBegintime;
	}

	public void setApplyBegintime(Date applyBegintime) {
		this.applyBegintime = applyBegintime;
	}

	public Date getApplyEndtime() {
		return applyEndtime;
	}

	public void setApplyEndtime(Date applyEndtime) {
		this.applyEndtime = applyEndtime;
	}

	public String getApplyContact() {
		return applyContact;
	}

	public void setApplyContact(String applyContact) {
		this.applyContact = applyContact;
	}

	public String getApplyRemarks() {
		return applyRemarks;
	}

	public void setApplyRemarks(String applyRemarks) {
		this.applyRemarks = applyRemarks;
	}
    
}
