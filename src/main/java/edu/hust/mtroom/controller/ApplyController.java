package edu.hust.mtroom.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.hust.mtroom.bean.ApplyForm;
import edu.hust.mtroom.bean.MtApply;
import edu.hust.mtroom.common.MyResEntity;
import edu.hust.mtroom.service.ApplyService;

@RestController
public class ApplyController {

	@Autowired
	private ApplyService applyService;
	
	@RequestMapping("/insert")
	public MyResEntity insert(HttpSession session,ApplyForm applyForm){
		applyService.insert(applyForm);
		return new MyResEntity();
	}
	@RequestMapping("/applyInfo")
	public MyResEntity applyInfo(String roomId,String dateStr){
//		dateStr="2017-11-27";
		List<MtApply>list=applyService.selectByDate(roomId,dateStr);
		return new MyResEntity(list);
	}
	
}
