package edu.hust.mtroom.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.hust.mtroom.bean.MtRoom;
import edu.hust.mtroom.common.MyResEntity;
import edu.hust.mtroom.service.RoomService;

@RestController
public class RoomController {

	@Autowired
	private RoomService roomService;
	
	@RequestMapping("/roomList")
	public MyResEntity roomList(String unitId){
		List<MtRoom> roomList=roomService.selectByUnitId(unitId);
		return new MyResEntity(roomList);
	}
	
	@RequestMapping("/roomDetail")
	public MyResEntity roomDetail(String resourceId){
		MtRoom room=roomService.selectByRoomId(resourceId);
		return new MyResEntity(room);
	}
	
}
