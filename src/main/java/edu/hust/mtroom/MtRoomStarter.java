package edu.hust.mtroom;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import org.jasig.cas.client.session.SingleSignOutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.util.ResourceUtils;

import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.neusoft.cas.AuthenticationFilter;
import com.neusoft.cas.Cas20ProxyReceivingTicketValidationFilter;

import edu.hust.mtroom.common.SysParamFilter;

@ComponentScan(basePackages={"edu.hust.mtroom"})  
@SpringBootApplication
public class MtRoomStarter {
	
	@Autowired
	private Environment env;
	
	Logger log=LoggerFactory.getLogger(MtRoomStarter.class);
	public static void main(String[] args) {
		SpringApplication.run(MtRoomStarter.class, args);
	}
	
	//该过滤器用于实现单点登出功能
	/*@Bean
    public FilterRegistrationBean singleSignOutFilter() {
    	FilterRegistrationBean f = new FilterRegistrationBean(new SingleSignOutFilter());
        f.addUrlPatterns("/*");  
        f.setName("SingleSignOutFilter");
        return f;  
    }
	//判断是否是保护资源 是否使用微信浏览器
	@Bean
    public FilterRegistrationBean casAuthenticationFilter() {
    	FilterRegistrationBean f = new FilterRegistrationBean(new AuthenticationFilter());
        f.addUrlPatterns("/*");  
        f.setName("CASAuthenticationFilter");
        return f;  
    }
	//该过滤器负责对Ticket的校验工作
	@Bean
    public FilterRegistrationBean cASValidationFilter() {
    	FilterRegistrationBean f = new FilterRegistrationBean(new Cas20ProxyReceivingTicketValidationFilter());
        f.addUrlPatterns("/*");  
        f.setName("CASValidationFilter");
        return f;  
    }
	//userid塞入session
	@Bean
    public FilterRegistrationBean sysParamFilter() {
    	FilterRegistrationBean f = new FilterRegistrationBean(new SysParamFilter());
        f.addUrlPatterns("/*");  
        f.setName("SysParamFilter");
        return f;  
    }*/
	
	//druid过滤器
	@Bean
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean f = new FilterRegistrationBean();
		f.setFilter(new WebStatFilter());
		f.addUrlPatterns("/*");
		f.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		f.addInitParameter("profileEnable", "true");
		f.addInitParameter("principalCookieName", "USER_COOKIE");
		f.addInitParameter("principalSessionName", "USER_SESSION");
		f.setName("druidFilter");
		return f;
	}
	//druid servlet
	@Bean
	public ServletRegistrationBean druidServlet() {
		ServletRegistrationBean reg = new ServletRegistrationBean();
		reg.setServlet(new StatViewServlet());
		reg.addUrlMappings("/druid/*");
		
		reg.addInitParameter("loginUsername", env.getProperty("druid.username"));
		reg.addInitParameter("loginPassword", env.getProperty("druid.password"));
		return reg;
	}
	
	//初始化bean读取err.properties
    @Bean
    public Properties errConfig() throws IOException {
    	URL u=ResourceUtils.getURL("classpath:err.properties");
    	InputStream stream=u.openStream();
    	Properties pro = new Properties();
    	InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(stream,"UTF-8");
	    	pro.load(isr);
		} finally{
			isr.close();
		}
        return pro;
    }
    
}
