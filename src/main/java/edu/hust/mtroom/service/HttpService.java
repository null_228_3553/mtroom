package edu.hust.mtroom.service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;

@Component
public class HttpService {
	private static final Logger log = LoggerFactory.getLogger(HttpService.class);
	private RestTemplate restTemplate;
	
	public HttpService(){
		restTemplate=new RestTemplate();
		//删除旧的StringConverter，添加UTF8的StringConverter
		List<HttpMessageConverter<?>> converterList=restTemplate.getMessageConverters();
        for (int i=0;i<converterList.size();i++) {
            if (converterList.get(i).getClass() == StringHttpMessageConverter.class) {
            	converterList.set(i, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            	break;
            }
        }
	}
	
	public String get(String url){
		String res=restTemplate.getForObject(url, String.class);
		log.info("接口：{}",url);
		log.info("返回：{}",res);
		return res;
	}
	
	
	public String post(String url,Map<String,Object> paraMap){
		HttpHeaders headers = new HttpHeaders();
		MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
		headers.setContentType(type);
		headers.add("Accept", MediaType.APPLICATION_JSON.toString());
		
		String jsonStr = JSON.toJSONString(paraMap);
		HttpEntity<String> formEntity = new HttpEntity<String>(jsonStr, headers);
		String ret=restTemplate.postForObject(url, formEntity, String.class, paraMap);
		return ret;
	}
}
