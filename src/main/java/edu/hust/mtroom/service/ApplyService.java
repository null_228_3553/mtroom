package edu.hust.mtroom.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.hust.mtroom.bean.ApplyForm;
import edu.hust.mtroom.bean.MtApply;
import edu.hust.mtroom.dao.MtApplyMapper;

@Component
public class ApplyService {
//	private static final Logger log = LoggerFactory.getLogger(ApplyService.class);

	@Autowired
	private MtApplyMapper mtApplyMapper;

	public void insert(ApplyForm applyForm) {
		MtApply mtApply=new MtApply();
		BeanUtils.copyProperties(applyForm, mtApply);
		
		String resourceId=new Date().toString();
		mtApply.setResourceId(resourceId);
		mtApply.setCreateTime(new Date());
		mtApply.setCheckStatus("2");//待审核
		mtApplyMapper.insertSelective(mtApply);
	}

	public List<MtApply> selectByDate(String roomId, String dateStr) {
		List<MtApply> list=mtApplyMapper.selectByDate(roomId, dateStr);
		return list;
	}
}
