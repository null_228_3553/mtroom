package edu.hust.mtroom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.hust.mtroom.bean.MtRoom;
import edu.hust.mtroom.dao.MtRoomMapper;

@Component
public class RoomService {
//	private static final Logger log = LoggerFactory.getLogger(ApplyService.class);

	@Autowired
	private MtRoomMapper mtRoomMapper;


	public List<MtRoom> selectByUnitId(String unitId) {
		List<MtRoom> list=mtRoomMapper.selectByUnitId(unitId);
		return list;
		}


	public MtRoom selectByRoomId(String resourceId) {
		MtRoom room=mtRoomMapper.selectByPrimaryKey(resourceId);
		return room;
	}
}
