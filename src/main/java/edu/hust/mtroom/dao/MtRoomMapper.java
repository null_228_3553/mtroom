package edu.hust.mtroom.dao;

import java.util.List;

import edu.hust.mtroom.bean.MtRoom;

public interface MtRoomMapper {
    int deleteByPrimaryKey(String resourceId);

    int insert(MtRoom record);

    int insertSelective(MtRoom record);

    MtRoom selectByPrimaryKey(String resourceId);
	List<MtRoom> selectByUnitId(String unitId);

    int updateByPrimaryKeySelective(MtRoom record);

    int updateByPrimaryKey(MtRoom record);

}