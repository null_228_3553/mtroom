package edu.hust.mtroom.dao;

import edu.hust.mtroom.bean.MtImage;

public interface MtImageMapper {
    int insert(MtImage record);

    int insertSelective(MtImage record);
}