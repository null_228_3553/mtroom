package edu.hust.mtroom.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import edu.hust.mtroom.bean.MtApply;

public interface MtApplyMapper {
    int deleteByPrimaryKey(String resourceId);

    int insert(MtApply record);

    int insertSelective(MtApply record);

    MtApply selectByPrimaryKey(String resourceId);

    int updateByPrimaryKeySelective(MtApply record);

    int updateByPrimaryKey(MtApply record);

	List<MtApply> selectByDate(@Param("roomId")String roomId, @Param("dateStr")String dateStr);
}