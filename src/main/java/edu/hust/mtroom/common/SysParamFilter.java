package edu.hust.mtroom.common;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jasig.cas.client.validation.Assertion;



/**
 * Servlet Filter implementation class SysParamFilter
 */
public class SysParamFilter implements Filter {


	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest=(HttpServletRequest)request;
		HttpSession session=httpRequest.getSession();
		Assertion a=(Assertion)session.getAttribute("_const_cas_assertion_");
		if(a!=null){
			String userId=a.getPrincipal().getName();
			session.setAttribute("userId", userId);
		}
//		session.setAttribute("userId", "U201315547");//2个号U201614656  //zhupai
//		session.setAttribute("userId", "U201414101");//1个自费号
//		session.setAttribute("userId", "U201614456");//1个教学号
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

}
