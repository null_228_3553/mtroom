package edu.hust.mtroom.common;

public class HkException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private String code;

	private String message;
	
	public HkException(String code) {
		this.code = code;
	}
	public HkException(String code,String message) {
		this.code = code;
		this.message=message;
	}

	public String getCode() {
		return code;
	}
	public String getMessage() {
		return message;
	}

}
